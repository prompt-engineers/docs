Welcome to the promptengineers.ai documentation!
===================================

.. raw:: html

   <div style="margin-bottom: 60px; display:flex; justify-content:center; align-items:center; height:100%; width:100%;">
      <iframe width="560" height="315" src="https://www.youtube.com/embed/HdD5eS7LeSI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
   </div>


Prompt Engineers is a code generation and development tool built around Large Language Models.
It is a text-based AI platform for enabling developers to work faster by performing
less searches and getting the right answer as early as possible in order to be agile
between contexts when developing code.

.. _Prompt Engineers API: https://api.promptengineers.ai/docs

Check out the :doc:`usage` section for further information, including
how to :ref:`installation` the project.

.. note::

   This project is under active development.

Contents
--------

.. toctree::

   usage
   features
   api
   
