Features
=========

Loaders
-------
Text Loader
~~~~~~~~~~~~~
.. raw:: html

   <div style="margin-bottom: 60px; display:flex; justify-content:center; align-items:center; height:100%; width:100%;">
      <iframe width="560" height="315" src="https://www.youtube.com/embed/Krv-Pooeync" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
   </div>

Web Page Laoder
~~~~~~~~~~~~~~~~
.. raw:: html

   <div style="margin-bottom: 60px; display:flex; justify-content:center; align-items:center; height:100%; width:100%;">
      <iframe width="560" height="315" src="https://www.youtube.com/embed/bB_b4sC3NHU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
   </div>