API
=========

.. _api_endpoint:

API Endpoint
------------

The Prompt Engineers API Document is at `Prompt Engineers`_.

.. _Prompt Engineers: https://dev-api.promptengineers.ai/redoc


Authentication
--------------

To authenticate your requests to the Prompt Engineers API, include an API key in the `Authorization` header with the `Bearer` authentication scheme.

.. code-block:: text

   Authorization: Bearer YOUR_API_KEY

Replace `YOUR_API_KEY` with your actual API key.

Chat Completion
---------------

To perform a chat completion with the Prompt Engineers API, send a POST request to the chat completion endpoint with a JSON payload containing a list of messages.

.. code-block:: console

   curl -X 'POST' \
     'https://dev-api.promptengineers.ai/api/v1/openai/chat-completion' \
     -H 'accept: application/json' \
     -H 'Authorization: Bearer YOUR_API_KEY' \
     -H 'Content-Type: application/json' \
     -d '{
       "messages": [
         {
           "role": "system",
           "content": "You are a helpful assistant."
         },
         {
           "role": "user",
           "content": "Who won the world series in 2020?"
         },
         {
           "role": "assistant",
           "content": "The Los Angeles Dodgers won the World Series in 2020."
         },
         {
           "role": "user",
           "content": "Where was it played?"
         }
       ]
     }'

Replace `YOUR_API_KEY` with your actual API key in the `Authorization` header.

The request payload should contain a list of messages, with each message having a `role` ("system", "user", or "assistant") and a `content` field. The conversation begins with a system message, followed by alternating user and assistant messages.

The response will be a JSON object containing the assistant's reply:

.. code-block:: json

   {
      "completion": {
         "role": "assistant",
         "content": "The 2020 World Series was played at Globe Life Field in Arlington, Texas."
      },
      "usage": 75
   }
